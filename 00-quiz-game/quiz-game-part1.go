package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"strings"
)

type question struct {
	q string
	a string
}

func main() {
	csvFile := flag.String("csv", "problems.csv", "CSV formatted file; 'question,answer'")
	flag.Parse()

	var file *os.File
	var err error
	if file, err = os.Open(*csvFile); err != nil {
		exit(fmt.Sprintf("Failed to open CSV file: %s", *csvFile))
	}
	defer file.Close()

	r := csv.NewReader(file)

	var data [][]string
	if data, err = r.ReadAll(); err != nil {
		exit("Failed to read CSV file.")
	}
	questions := parseCSV(data)

	reader := bufio.NewReader(os.Stdin)

	var correct int
	for i, question :=  range questions {
		fmt.Printf("Question %d: %s = ", i+1, question.q)
		userIn, _ := reader.ReadString('\n')
		userIn = strings.Replace(userIn, "\n", "", -1)

		if strings.Compare(userIn, question.a) == 0 {
			correct++
		}
	}

	fmt.Printf("You scored: %d/%d\n", correct, len(questions))
}

func exit(str string) {
	fmt.Fprintf(os.Stderr, "%s\n", str)
	os.Exit(2)
}

func parseCSV(lines [][]string) []question {
	ret := make([]question, len(lines))

	for i, line := range lines {
		ret[i] = question {
			q: line[0],
			a: strings.TrimSpace(line[1]),
		}
	}

	return ret
}