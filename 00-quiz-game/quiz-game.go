package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

type question struct {
	q string
	a string
}

var (
	// flags
	csvFile  string
	duration uint
	shuffle  bool
)

func init() {
	rand.Seed(time.Now().UnixNano())

	// move the flag parsing to the init function as this is something
	// we want to do everytime to setup the program.
	flag.BoolVar(&shuffle, "shuffle", false, "When set, will shuffle the order of the problems.")
	flag.BoolVar(&shuffle, "s", false, "When set, will shuffle the order of the problems. (shorthand)")

	flag.UintVar(&duration, "duration", 30, "Duration of the quiz in seconds")
	flag.StringVar(&csvFile, "csv", "problems.csv", "CSV formatted file; 'question,answer'")
}

func main() {
	flag.Parse()

	var file *os.File
	var err error
	if file, err = os.Open(csvFile); err != nil {
		exit(fmt.Sprintf("Failed to open CSV file: %s", csvFile))
	}
	defer file.Close()

	r := csv.NewReader(file)

	var data [][]string
	if data, err = r.ReadAll(); err != nil {
		exit("Failed to read CSV file.")
	}
	questions := parseCSV(data)
	if shuffle {
		rand.Shuffle(len(questions), func(i, j int) { questions[i], questions[j] = questions[j], questions[i] })
	}

	reader := bufio.NewReader(os.Stdin)

	var correct int
	timer := time.NewTimer(time.Duration(duration) * time.Second)

	for i, question := range questions {
		fmt.Printf("Question %d: %s = ", i+1, question.q)

		userInChan := make(chan string)
		go func() {
			userIn, _ := reader.ReadString('\n')
			userIn = strings.Replace(userIn, "\n", "", -1)
			userInChan <- userIn
		}()

		select {
		case <-timer.C:
			fmt.Println("\nTime's up...")
			fmt.Printf("You scored: %d/%d\n", correct, len(questions))
			return
		case answer :=  <-userInChan:
			if strings.Compare(answer, question.a) == 0 {
				correct++
			}
		}
	}

	fmt.Printf("You scored: %d/%d\n", correct, len(questions))
}

func exit(str string) {
	fmt.Fprintf(os.Stderr, "%s\n", str)
	os.Exit(2)
}

func parseCSV(lines [][]string) []question {
	ret := make([]question, len(lines))

	for i, line := range lines {
		ret[i] = question{
			q: line[0],
			a: strings.TrimSpace(line[1]),
		}
	}

	return ret
}
